package com.nahtredn.helpers;

public class Queries {

    static public String queryPasswordFor(String username){
        return String.format("{ \n" +
                "  result(func: eq(email, \"%s\")){ \n" +
                "    uid\n" +
                "    password\n" +
                "  }\n" +
                "}", username);
    }

    static public String queryCreateAccountFor(String username, String password){
        return String.format("{ \n" +
                "  set {\n" +
                "    _:person <email> \"%s\" .\n" +
                "    _:person <password> \"%s2\" .\n" +
                "  }\n" +
                "}", username, password);
    }
}
