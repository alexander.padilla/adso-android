package com.nahtredn.adso;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.nahtredn.utilities.Dgraph;
import com.nahtredn.utilities.Messenger;
import com.nahtredn.utilities.PreferencesProperties;
import com.nahtredn.utilities.RealmController;
import com.nahtredn.utilities.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    // EditText inputUsername, inputPassword;
    // TextInputLayout layoutUsername, layoutPassword;
    String username, password;
    String message;
    int tries = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public void handleClickLogin(View view){

        if (view.getId() == R.id.btn_sing_in){

            EditText inputUsername = findViewById(R.id.username);
            TextInputLayout labelUsername = findViewById(R.id.label_username);

            if (!Validator.with(this).validateEmail(inputUsername,labelUsername)){
                return;
            }

            EditText inputPassword = findViewById(R.id.password);
            TextInputLayout labelPassword = findViewById(R.id.label_password);

            if (!Validator.with(this).validateText(inputPassword, labelPassword)){
                return;
            }

            username = inputUsername.getText().toString().trim();
            password = inputPassword.getText().toString().trim();

            new LogIn().execute();
        }
    }

    private void showSnackForRegistry(){
        Snackbar.make(findViewById(android.R.id.content), "El correo no está registrado", Snackbar.LENGTH_LONG)
                .setAction("REGISTRAR", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        registry();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.darker_gray))
                .show();
    }

    private void showSnackForRestore(){
        Snackbar.make(findViewById(android.R.id.content), "¿Olvidaste tu contraseña?", Snackbar.LENGTH_LONG)
                .setAction("RECUPERAR", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Messenger.with(LoginActivity.this).showMessage("Recuperando cuenta");
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.darker_gray))
                .show();
    }

    private void registry(){
        Intent intent = new Intent(LoginActivity.this, RegistryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplication().startActivity(intent);
    }

    private void logIn(String uid){
        RealmController.with(LoginActivity.this).login(uid, username, password);
        Messenger.with(this).showMessage("Bienvenido");
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplication().startActivity(intent);
    }

    class LogIn extends AsyncTask<String, String, String> {
        private Map<String, String> credentials;
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this, ProgressDialog.THEME_HOLO_DARK);
            pDialog.setMessage("Iniciando sesión...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... f_url) {
            credentials = Dgraph.queryCredentials(username);
            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            if (credentials != null) {
                if (credentials.containsKey("password")) {
                    if (!password.equalsIgnoreCase(credentials.get("password"))){
                        if(tries < 3) {
                            Validator.with(LoginActivity.this)
                                    .setErrorMessage((EditText) findViewById(R.id.password),
                                            (TextInputLayout) findViewById(R.id.label_password),
                                            "Contraseña incorrecta");
                            tries += 1;
                        } else {
                            showSnackForRestore();
                        }
                    } else {
                        logIn(credentials.get("uid"));
                    }
                } else {
                    showSnackForRegistry();
                }
            } else {
                showSnackForRegistry();
            }
        }
    }
}
