package com.nahtredn.adso;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lowagie.text.DocumentException;
import com.nahtredn.adapters.VacancyAdapter;
import com.nahtredn.entities.Vacancy;
import com.nahtredn.fragments.DataFragment;
import com.nahtredn.fragments.VacancyFragment;
import com.nahtredn.utilities.Messenger;
import com.nahtredn.utilities.PDF;
import com.nahtredn.utilities.PreferencesProperties;
import com.nahtredn.utilities.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AdView mAdView;
    private ProgressDialog pDialog;
    private String result;
    private String errorMesage = "";
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!RealmController.with(this).isLogged()){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
            this.finish();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null){
            toolbar.setTitle(getString(R.string.title_activity_data));
            setSupportActionBar(toolbar);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        DataFragment dataFragment = (DataFragment)
                getSupportFragmentManager().findFragmentById(R.id.data_items_data_container);

        if (dataFragment == null) {
            dataFragment = DataFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.data_items_data_container, dataFragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_preview) {
            if(hasPermissonFor(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                GeneratePdf task = new GeneratePdf(2);
                task.execute();
            } else {
                Messenger.with(MainActivity.this).showMessage("ADSO requiere permisos de escritura para crear y guardar la foto de perfil y la solicitud digital.");
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            }
        }

        if (id == R.id.nav_share) {
            if(hasPermissonFor(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                GeneratePdf task = new GeneratePdf(1);
                task.execute();
                new SendData().execute("https://naht-redn-dev.cloud.tyk.io/adso");
            } else {
                Messenger.with(MainActivity.this).showMessage("ADSO requiere permisos de escritura para crear y guardar la foto de perfil y la solicitud digital.");
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            }
        }

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        }

        if (id == R.id.nav_close_sesion) {
            RealmController.with(this).save(PreferencesProperties.IS_LOGGED.toString(), false);
            this.finish();
        }

        if (id == R.id.nav_exit) {
            this.finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Messenger.with(this).showMessage("Excelente, ADSO ya puede crear los archivos necesarios para su funcionamiento");
                } else {
                    Messenger.with(this).showMessage("Sin permisos de escritura, ADSO no puede funcionar");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean hasPermissonFor(String permission){
        int code = getApplicationContext().getPackageManager().checkPermission(permission,
                getApplicationContext().getPackageName());
        return code == PackageManager.PERMISSION_GRANTED;
    }

    class GeneratePdf extends AsyncTask<String, String, String> {
        private int action;

        public GeneratePdf(int action) {
            this.action = action;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this, ProgressDialog.THEME_HOLO_DARK);
            pDialog.setMessage("Generando solicitud...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... f_url) {
            try {

                errorMesage = PDF.with(getApplication()).generaSolicitud();
                if(!errorMesage.equals("")){
                    this.cancel(true);
                }
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            File f = new File(RealmController.with(getApplication()).find(PreferencesProperties.PATH_FILE.toString()));
            if (f.exists()){
                if (action == 1){
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + f.getAbsolutePath()));
                    intent.setType("application/pdf");
                    startActivity(Intent.createChooser(intent, "Compartir mediante"));
                }

                if (action == 2){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(f), "application/pdf");
                    startActivity(Intent.createChooser(intent, "Visualizar mediante"));
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            pDialog.dismiss();
            Messenger.with(MainActivity.this).showMessage(errorMesage);
        }
    }

    class SendData extends AsyncTask<String, String, String>{

        private String result;

        @Override
        protected String doInBackground(String... strings) {
            try{
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type","application/json");
                connection.setRequestProperty("Accept","application/json");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                JSONObject jsonParam = RealmController.with(MainActivity.this).getUserData();

                if(!jsonParam.toString().isEmpty()) {
                    DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                    dataOutputStream.writeBytes(jsonParam.toString());
                    dataOutputStream.flush();
                    dataOutputStream.close();
                    InputStream input = connection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input, "utf-8"), 8);
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = null;
                    while ((line = bufferedReader.readLine()) != null){
                        stringBuilder.append(line + "\n");
                    }
                    input.close();
                    result = stringBuilder.toString();
                }

                connection.disconnect();
            } catch (IOException ioe){
                Log.w("Adso main", "Error: " + ioe.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.w("Adso main", "Result: " + result);
        }
    }
}